<?php

namespace App\Http\Controllers\Api;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return response()->json(Task::all());
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'status' => ['required', Rule::in(['pending', 'in_progress', 'completed'])],
            'due_date' => 'required|date',
        ]);

        $task = Task::create($validatedData);

        return response()->json($task, 201);
    }

    public function show($id)
    {
        $task = Task::with('getuser')->find($id);

        return response()->json($task);
    }

    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        $validatedData = $request->validate([
            'title' => 'sometimes|required|string|max:255',
            'description' => 'sometimes|required|string',
            'status' => ['sometimes', 'required', Rule::in(['pending', 'in_progress', 'completed'])],
            'due_date' => 'sometimes|required|date',
        ]);

        $task->update($validatedData);

        return response()->json($task);
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return response()->json(null, 204);
    }

    public function indexByStatus($status)
    {
        $tasks = Task::where('status', $status)->get();
        return response()->json($tasks);
    }

    public function indexByDueDate($due_date)
    {
        $tasks = Task::where('due_date', $due_date)->get();
        return response()->json($tasks);
    }
}
